import pyaudio
import numpy as np
import matplotlib.pyplot as plt
import wave
from matplotlib.animation import FuncAnimation
from scipy.signal import butter, lfilter, find_peaks
import sys

#FORMAT = pyaudio.paInt16
#CHANNELS = 1
RATE = 16000 # sample frequency
CHUNK = 400 # of samples per read
#Initialize PyAudio
audio = pyaudio.PyAudio()
current_valid_samples = 0
DTMF_LOW = (697, 770, 852, 941)
DTMF_HIGH = (1209, 1336, 1477, 1633)
DTMF_INDEX_TAB = {
    #(DTMF_LOW, DTMF_HIGH)
    (0, 0): 1,
    (0, 1): 2,
    (0, 2): 3,
    (0, 3): 'A',
    (1, 0): 4,
    (1, 1): 5,
    (1, 2): 6,
    (1, 3): 'B',
    (2, 0): 7,
    (2, 1): 8,
    (2, 2): 9,
    (2, 3): 'C',
    (3, 0): '*',
    (3, 1): 0,
    (3, 2): '#',
    (3, 3): 'D',
}

#Build filter template that will be applied later in the code
def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

#WAV
try:
    wf = wave.open(sys.argv[1], 'rb')
except:
    exit(-1)

stream = audio.open(format=audio.get_format_from_width(wf.getsampwidth()),
            channels=wf.getnchannels(),
            rate=wf.getframerate(), output=True)

#Decode low and high frequency pair to digit
def identify_frame_digit(freq_low, freq_high):
    delta = 1000000
    index_low = 0
    index_big = 0
    #Find the closest frequency for the minimum range
    for i in range(0, 4):
        if abs(freq_low - DTMF_LOW[i]) < delta:
            delta = abs(freq_low - DTMF_LOW[i])
            index_low = i
    delta = 1000000
    #Find the closest frequency for the maximum range
    for i in range(0, 4):
        if abs(freq_high - DTMF_HIGH[i]) < delta:
            delta = abs(freq_high - DTMF_HIGH[i])
            index_big = i
    #Decode found frequencies to digit
    return(str(DTMF_INDEX_TAB[(index_low, index_big)]))


def plot_wave(i):
    #data = stream.read(CHUNK, exception_on_overflow=False)

    #Grab a chunk of samples from the WAV file
    data = wf.readframes(CHUNK)
    #Write the chunk to stream in order to play the sound
    stream.write(data)
        
    try:
        #Exit gracefully and prevent a crash in the eventuallty that the WAV stream ends
        #Convert to numbers from the pyaudio format
        waveData = wave.struct.unpack("%dh"%(CHUNK), data)
    except:
        #Cleanup of the audio library
        stream.stop_stream()
        stream.close()
        audio.terminate()
        exit(0)
    #Convert to numpy data types
    sample_data = np.array(waveData)
    # Apply bandpass filterting between 600 Hz and 1700 Hz
    sample_data = butter_bandpass_filter(sample_data, 600, 1700, RATE, order=5)

    #Do the fourier transform on the current CHUNK of samples
    fftData = np.abs(np.fft.rfft(sample_data))
    fftData = np.log(fftData + 1)
    #Build the vector of frequencies that is related to the computed FFT
    fftFreq = np.fft.rfftfreq(CHUNK, 1./RATE)
    #Plot the FFT graph
    plt.cla()
    plt.ylim(top=20)
    plt.xlim(0, RATE * 0.5)
    plt.autoscale(False)
    plt.plot(fftFreq, fftData, color='green')

    (peaks, properties) = find_peaks(fftData, height=12, distance=7)
    #Group amplitude, frequency and peak data into a single data structure
    peak_data = list(zip(peaks, fftData[peaks], fftFreq[peaks]))
    #print(fftFreq[1], fftFreq[2], fftFreq[3])
    #Sort peaks decreasingly by amplitude in order to find the biggest amplitude ones
    peak_data = sorted(peak_data, key=lambda item: item[1], reverse=True)
    #If continuing a digit then add to the average total and increment sample count
    if len(peak_data) >= 2:
        plot_wave.current_valid_samples += 1
        #Check if the first peak is the lower or high frequency and att to the proper total
        if peak_data[0][2] > peak_data[1][2]:
            plot_wave.big_freq += peak_data[0][2]
            plot_wave.small_freq += peak_data[1][2]
        else:
            plot_wave.big_freq += peak_data[1][2]
            plot_wave.small_freq += peak_data[0][2]
    else:
        #Reset the static variables preparing for the next digit
        #This else branch means the DTMF pulse ended and we are in a period of no signal
        if plot_wave.current_valid_samples > 0:
            #Average previous samples over the duration of the DTMF pulse
            plot_wave.big_freq = plot_wave.big_freq / plot_wave.current_valid_samples
            plot_wave.small_freq = plot_wave.small_freq / plot_wave.current_valid_samples
            #print('Digit chunk count: ', plot_wave.current_valid_samples)
            #print('Big freq: ' + str(plot_wave.big_freq))
            #print('Small freq: ' + str(plot_wave.small_freq))
            print('Digit is: ' + identify_frame_digit(plot_wave.small_freq, plot_wave.big_freq))
            #Reset vriables for the next pulse
            plot_wave.current_valid_samples = 0
            plot_wave.small_freq = 0
            plot_wave.big_freq = 0
    #print(peak_data[:2])
    for index in peaks:
        plt.plot(fftFreq[index], fftData[index], 'rp', markersize=2)

plot_wave.current_valid_samples = 0
plot_wave.small_freq = 0
plot_wave.big_freq = 0
ani = FuncAnimation(plt.gcf(), plot_wave, interval=10)

plt.show()
stream.stop_stream()
stream.close()
audio.terminate()